package main

import (
	"fmt"
	"gin/api/listen"
	"gin/api/models"
	"gin/cmd"
	"gin/utils/config"
	"gin/utils/console"
	"gin/utils/global"
	"gin/utils/jwt"
	"gin/utils/logging"
	"gin/utils/redis"
	"github.com/spf13/cobra"
	"os"
)

func init() {
	global.GIN_VP = config.Viper()
	global.GIN_LOG = logging.SetupLogger()
	models.Setup()
	logging.Setup()
	redis.Setup()
	jwt.Setup()
	listen.Setup() //redis订阅
}

// @title gin-shop  API
// @version 1.0
// @description gin-shop商城后台管理系统
// @termsOfService https://gitee.com/guchengwuyue/gin-shop
// @license.name apache2
func main() {
	var rootCmd = &cobra.Command{
		Use:   "shop=gin",
		Short: "gin系统r",
		Long:  "will run serve command",

		PersistentPreRun: func(cmd *cobra.Command, args []string) {

		},
	}

	rootCmd.AddCommand(cmd.CmdServe)

	// 配置默认运行 Web 服务
	cmd.RegisterDefaultCmd(rootCmd, cmd.CmdServe)

	// 注册全局参数，--env
	cmd.RegisterGlobalFlags(rootCmd)

	// 执行主命令
	if err := rootCmd.Execute(); err != nil {
		console.Exit(fmt.Sprintf("Failed to run app with %v: %s", os.Args, err.Error()))
	}

}
