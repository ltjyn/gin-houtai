package cmd

import (
	"fmt"
	"gin/routers"
	"gin/utils/console"
	"gin/utils/global"
	"github.com/gin-gonic/gin"
	"github.com/spf13/cobra"
	"net/http"
	"time"
)

var CmdServe = &cobra.Command{
	Use:   "serve",
	Short: "start web server",
	Run:   runweb,
	Args:  cobra.NoArgs,
}

func runweb(cmd *cobra.Command, args []string) {
	gin.SetMode(global.GIN_CONFIG.Server.RunMode)

	routersInit := routers.InitRouter()
	endPoint := fmt.Sprintf(":%d", global.GIN_CONFIG.Server.HttpPort)
	maxHeaderBytes := 1 << 20

	server := &http.Server{
		Addr:           endPoint,
		Handler:        routersInit,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: maxHeaderBytes,
	}

	global.GIN_LOG.Info("[info] start http server listening %s", endPoint)

	err := server.ListenAndServe()
	if err != nil {
		global.GIN_LOG.Error(err.Error())
		console.Exit("start server error:" + err.Error())
	}
}
