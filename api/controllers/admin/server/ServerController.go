package server

import (
	"gin/api/models"
	"gin/api/service/server_service"
	"gin/utils/app"
	"gin/utils/constant"
	"gin/utils/util"
	"github.com/gin-gonic/gin"
	"github.com/unknwon/com"
	"net/http"
)

// 岗位api
type ServerController struct {
}

// @Title 服务器列表
// @Description 服务器列表
// @Success 200 {object} app.Response
// @router / [get]
func (e *ServerController) GetServers(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
	)
	status := com.StrTo(c.DefaultQuery("status", "0")).MustInt()
	name := c.DefaultQuery("blurry", "")
	serverService := server_service.Server{
		Status:   status,
		Name:     name,
		PageSize: util.GetSize(c),
		PageNum:  util.GetPage(c),
	}
	vo := serverService.GetAll()
	appG.Response(http.StatusOK, constant.SUCCESS, vo)
}

// @Title 玩家列表
// @Description 玩家详细信息
// @Success 200 {object} app.Response
// @router / [post]
func (e *ServerController) Post(c *gin.Context) {
	var (
		model models.Server
		appG  = app.Gin{C: c}
	)

	httpCode, errCode := app.BindAndValid(c, &model)
	if errCode != constant.SUCCESS {
		appG.Response(httpCode, errCode, nil)
		return
	}
	jobService := server_service.Server{
		M: &model,
	}

	if err := jobService.Insert(); err != nil {
		appG.Response(http.StatusInternalServerError, constant.FAIL_ADD_DATA, nil)
		return
	}

	appG.Response(http.StatusOK, constant.SUCCESS, nil)

}

// @Title 岗位修改
// @Description 岗位修改
// @Success 200 {object} app.Response
// @router / [put]
func (e *ServerController) Put(c *gin.Context) {
	var (
		model models.Server
		appG  = app.Gin{C: c}
	)
	httpCode, errCode := app.BindAndValid(c, &model)
	if errCode != constant.SUCCESS {
		appG.Response(httpCode, errCode, nil)
		return
	}
	jobService := server_service.Server{
		M: &model,
	}

	if err := jobService.Save(); err != nil {
		appG.Response(http.StatusInternalServerError, constant.FAIL_ADD_DATA, nil)
		return
	}

	appG.Response(http.StatusOK, constant.SUCCESS, nil)
}

// @Title 服务器删除
// @Description 服务器删除
// @Success 200 {object} app.Response
// @router / [delete]
func (e *ServerController) Delete(c *gin.Context) {
	var (
		ids  []int64
		appG = app.Gin{C: c}
	)
	c.BindJSON(&ids)
	jobService := server_service.Server{Ids: ids}

	if err := jobService.Del(); err != nil {
		appG.Response(http.StatusInternalServerError, constant.FAIL_ADD_DATA, nil)
		return
	}

	appG.Response(http.StatusOK, constant.SUCCESS, nil)
}
