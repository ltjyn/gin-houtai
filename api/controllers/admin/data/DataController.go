package data

import (
	"gin/api/service/data_service"
	"gin/utils/app"
	"gin/utils/constant"
	"gin/utils/logging"
	"gin/utils/util"
	"github.com/gin-gonic/gin"
	"github.com/unknwon/com"
	"net/http"
)

// data api
type DataController struct {
}

// @Title 玩家列表
// @Description 玩家详细信息
// @Success 200 {object} app.Response
// @router / [get]
func (e *DataController) GetAll(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
	)
	// fmt.Println("sids = %v ", c.Request.URL.Query())
	uid := c.DefaultQuery("uid", "")
	name := c.DefaultQuery("nick_name", "")
	serverId := com.StrTo(c.DefaultQuery("sid", "")).MustInt()
	openId := c.DefaultQuery("open_id", "")
	dataService := data_service.Param{
		Uid:      uid,
		NickName: name,
		ServerId: serverId,
		OpenId:   openId,
		PageSize: util.GetSize(c),
		PageNum:  util.GetPage(c),
	}
	vo := dataService.GetList() // 少参数 ,

	appG.Response(http.StatusOK, constant.SUCCESS, vo)
}

func (e *DataController) GetOne(c *gin.Context) {
	var (
		appG = app.Gin{C: c}
	)
	serverId := com.StrTo(c.DefaultQuery("sid", "")).MustInt()
	uid := c.DefaultQuery("uid", "")
	logging.Info("sid = %d , uid = %s", serverId, uid)
	dataService := data_service.Param{
		ServerId: serverId,
		Uid:      uid,
	}
	vo := dataService.GenOne()
	appG.Response(http.StatusOK, constant.SUCCESS, vo)
}
