package data_service

import (
	"gin/api/models"
	"gin/api/models/vo"
)

type Param struct {
	Uid      string
	NickName string
	ServerId int
	OpenId   string
	PageNum  int
	PageSize int
}

// 订单列表 -1全部 默认为0未支付 1待发货 2待收货 3待评价 4已完成
func (d *Param) GetList() vo.ResultList {
	maps := make(map[string]interface{})
	if d.Uid != "" {
		maps["uid"] = d.Uid
	}
	if d.OpenId != "" {
		maps["open_id"] = d.OpenId
	}
	if d.NickName != "" {
		maps["nick_name"] = d.NickName
	}
	total, list := models.GetAllRoles(d.PageNum, d.PageSize, maps, d.ServerId)

	return vo.ResultList{Content: list, TotalElements: total}
}

func (d *Param) GenOne() vo.ResultList {
	oneUser := models.GetOneUser(d.Uid, d.ServerId)
	return vo.ResultList{Content: oneUser}
}
