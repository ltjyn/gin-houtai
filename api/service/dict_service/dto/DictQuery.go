package dto

import "gin/api/models/dto"

type DictQuery struct {
	dto.BasePage
	Blurry string
}