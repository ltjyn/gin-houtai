package server_service

import (
	"gin/api/models"
	"gin/api/models/vo"
)

type Server struct {
	Id   int
	Name string

	Status int

	PageNum  int
	PageSize int

	M *models.Server

	Ids []int64
}

// 获取一个服
func (d *Server) GetFirstServer() models.Server {
	return models.GetFirstServer()
}

// 获取一个服
func (d *Server) GetOneServer() models.Server {
	return models.GetOneServer(d.Id)
}

// 获取已开的服
func (d *Server) GetOpenServer() []models.Server {
	return models.GetOpenServer()
}

// 获取所有服
func (d *Server) GetAll() vo.ResultList {
	maps := make(map[string]interface{})
	if d.Name != "" {
		maps["name"] = d.Name
	}

	total, list := models.GetAllServers(d.PageNum, d.PageSize, maps)
	return vo.ResultList{Content: list, TotalElements: total}
}

func (d *Server) Insert() error {
	return models.AddServer(d.M)
}

func (d *Server) Save() error {
	return models.UpdateByServer(d.M)
}

func (d *Server) Del() error {
	return models.DelByServer(d.Ids)
}
