package listen

import "gin/utils/global"

func Setup() {
	var sub PSubscriber
	//fmt.Printf(global.GIN_CONFIG.Redis.Host)
	conn := PConnect(global.GIN_CONFIG.Redis.Host, global.GIN_CONFIG.Redis.Password)
	sub.ReceiveKeySpace(conn)
	sub.Psubscribe()
}
