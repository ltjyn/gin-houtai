package models

import "time"

type Server struct {
	Id        int32     `json:"id"`
	Sid       int       `json:"sid"`
	Time      time.Time `json:"time"`
	GmwebIp   string    `json:"gmweb_ip"`
	ServerIp  string    `json:"server_ip"`
	Recommend int8      `json:"recommend"`
	Name      string    `json:"name"`
	Status    int8      `json:"status"`
	Register  int8      `json:"register"`
	OpenState int8      `json:"open_state"`
	Test      int8      `json:"test"`
	BaseModel
}

func (Server) TableName() string {
	return "server"
}

func GetFirstServer() Server {
	var server Server
	db.First(&server)
	return server
}

func GetOneServer(id int) Server {
	var server Server
	db.Where("id = ?", id).First(&server)
	return server
}

func GetOpenServer() []Server {
	var server []Server
	db.Where("status = ?", 1).First(&server)
	return server
}

// get all
func GetAllServers(pageNUm int, pageSize int, maps interface{}) (int64, []Server) {
	var (
		total int64
		lists []Server
	)
	db.Model(&Server{}).Where(maps).Count(&total)
	db.Where(maps).Offset(pageNUm).Limit(pageSize).Find(&lists)

	return total, lists
}

func AddServer(s *Server) error {
	var err error
	if err = db.Create(s).Error; err != nil {
		return err
	}
	return err
}

func UpdateByServer(s *Server) error {
	var err error
	err = db.Debug().Save(s).Error
	if err != nil {
		return err
	}

	return err
}

func DelByServer(ids []int64) error {
	var err error
	err = db.Where("id in (?)", ids).Delete(&Server{}).Error
	if err != nil {
		return err
	}

	return err
}
