package models

import (
	"strconv"
	"strings"
	"time"
)

type GameCombinedServer struct {
	Id       int       `json:"id"`
	SidRange string    `json:"sid_range"`
	Time     time.Time `json:"time"`
	Title    string    `json:"title"`
	Content  string    `json:"content"`
	Desc     string    `json:"desc"`
	BaseModel
}

func (c *GameCombinedServer) TableName() string {
	return "combined_server"
}

func GetCombinedSId(sid int) int {
	mergeServer := []GameCombinedServer{}
	currentTime := time.Now().Format("2006-01-02 15:04:05")
	db.Where("time <= ?", currentTime).First(&mergeServer)
	tmpSid := sid
	for _, val := range mergeServer {
		arr := strings.Split(val.SidRange, ":")
		intStart, _ := strconv.Atoi(arr[0])
		intEnd, _ := strconv.Atoi(arr[1])
		if sid >= intStart && sid <= intEnd {
			tmpSid = intStart
		}
	}
	return tmpSid
}
