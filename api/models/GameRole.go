package models

import (
	"gin/utils/constant"
	"gin/utils/logging"
	"strconv"
	"time"
)

type GameRole struct {
	Id               int       `json:"id"`
	Uid              string    `json:"uid"`
	Serverid         int       `json:"serverid"`
	NickName         string    `json:"nick_name"`
	BaseLevel        string    `json:"base_level"`
	CreateDate       time.Time `json:"create_date"`
	LastLoginDate    time.Time `json:"last_login_date"`
	BanDate          time.Time `json:"ban_date"`
	BanChatDate      time.Time `json:"ban_chat_date"`
	MapLevel         int32     `json:"map_level"`
	MaxFightPower    int32     `json:"max_fight_power"`
	FightPower       int32     `json:"fight_power"`
	OpenId           string    `json:"open_id"`
	SubPlatform      string    `json:"sub_platform"`
	TowerLevel       int32     `json:"tower_level"`
	Vip              int       `json:"vip"`
	Money            int64     `json:"money"`
	Spar             int32     `json:"spar"`
	PayMoney         int       `json:"pay_money"`
	Zeny             int64     `json:"zeny"`
	TotalRecharge    float32   `json:"total_recharge"`
	LastRechargeTime time.Time `json:"last_recharge_time"`
}

func (GameRole) TableName() string {
	return "mt_gs_7001.role"
}

func GetAllRoles(pageNUm int, pageSize int, maps interface{}, sid int) (int64, []GameRole) {
	var (
		total int64
		data  []GameRole
	)

	if sid <= 0 {
		var server Server
		db.First(&server)
		sid = server.Sid
	}
	databaseName := constant.DB_PERFIX + strconv.Itoa(sid) + ".role"
	db.Table(databaseName).Model(&GameRole{}).Where(maps).Count(&total)
	db.Table(databaseName).Where(maps).Offset(pageNUm).Limit(pageSize).Order("id desc").Find(&data)

	return total, data
}

func GetOneUser(uid string, sid int) GameRole {
	sid = GetCombinedSId(sid)
	dbRole := constant.DB_PERFIX + strconv.Itoa(sid) + ".role"
	dbRoleOrderList := constant.DB_PERFIX + strconv.Itoa(sid) + ".role_order_list"

	var data GameRole
	sqlHeader := "SELECT r.*, SUM(p.rmb) total_recharge FROM " + dbRole + " r LEFT JOIN " + dbRoleOrderList
	sql := sqlHeader + " p ON r.uid = p.uid WHERE r.uid = " + uid + " GROUP BY r.uid"
	err := db.Raw(sql).Find(&data).Error
	if err != nil {
		logging.Error(err)
	}
	/*
		databaseName := constant.DB_PERFIX + strconv.Itoa(sid) + ".role"
		var data GameRole
		db.Table(databaseName).Where(maps).First(&data)
	*/
	return data
}
