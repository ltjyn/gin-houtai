package models

import (
	"gin/utils/constant"
	"strconv"
	"time"
)

type GameRoleOrderList struct {
	Id         int       `json:"id"`
	Uid        string    `json:"uid"`
	Rmb        int       `json:"rmb"`
	RewardList string    `json:"reward_list"`
	OrderId    string    `json:"order_id"`
	OrderDate  time.Time `json:"order_date"`
	GoodsType  int       `json:"goods_type"`
	GoodsId    int       `json:"goods_id"`
	SdkOrderId string    `json:"sdk_order_id"`
}

func (GameRoleOrderList) TableName() string {
	return "mt_gs_7001.role_order_list"
}

func GetAllRoleOrderList(pageNUm int, pageSize int, maps interface{}, sid int) (int64, []GameRoleOrderList) {
	var (
		total int64
		data  []GameRoleOrderList
	)

	// err := db.Raw("SELECT r.* FROM sys_role r, sys_users_roles u WHERE r.id = u.sys_role_id AND u.sys_user_id = ?", uid).Find(&lists).Error
	databaseName := constant.DB_PERFIX + strconv.Itoa(sid) + ".role_order_list"
	db.Table(databaseName).Model(&GameRole{}).Where(maps).Count(&total)
	db.Table(databaseName).Where(maps).Offset(pageNUm).Limit(pageSize).Order("id desc").Find(&data)
	return total, data
}
