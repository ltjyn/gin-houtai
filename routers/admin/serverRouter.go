package admin

import (
	"gin/api/controllers/admin/server"
	"gin/middleware"
	"github.com/gin-gonic/gin"
)

func RegisterServerRouters(r *gin.Engine) {
	serverController := server.ServerController{}
	serverRouter := r.Group("/server")
	serverRouter.Use(middleware.Jwt()).Use(middleware.Log())
	{
		serverRouter.GET("/list", serverController.GetServers)
		serverRouter.POST("/index", serverController.Post)
		serverRouter.PUT("/index", serverController.Put)
		serverRouter.DELETE("/index", serverController.Delete)
	}
}
