package admin

import (
	"gin/api/controllers/admin/data"
	"gin/middleware"
	"github.com/gin-gonic/gin"
)

func RegisterDataRouters(r *gin.Engine) {
	dataController := data.DataController{}
	dataRouter := r.Group("/data")
	dataRouter.Use(middleware.Jwt()).Use(middleware.Log())
	{
		dataRouter.GET("/role", dataController.GetAll)
		dataRouter.GET("/role/show", dataController.GetOne)
	}
}
