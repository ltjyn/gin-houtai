package routers

import (
	_ "gin/docs"
	"gin/middleware"
	"gin/routers/admin"
	"gin/utils/upload"
	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	"github.com/swaggo/gin-swagger"
	"net/http"
)

func InitRouter() *gin.Engine {
	r := gin.New()
	r.Use(gin.Logger())
	r.Use(gin.Recovery())
	r.Use(middleware.Cors())

	r.StaticFS("/upload/images", http.Dir(upload.GetImageFullPath()))
	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

	//注册后台路由
	admin.RegisterSystemRouters(r)
	admin.RegisterToolsRouters(r)
	admin.RegisterDataRouters(r)
	admin.RegisterServerRouters(r)
	//注册api 路由
	//api.RegisterApiRouters(r)

	return r
}
