package app

import (
	"github.com/astaxie/beego/validation"
	"gin/utils/global"
)

// MarkErrors logs error logs
func MarkErrors(errors []*validation.Error) {
	for _, err := range errors {
		global.GIN_LOG.Info(err.Key, err.Message)
	}

	return
}
