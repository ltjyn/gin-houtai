package constant

const (
	ContextKeyUserObj     = "authedUserObj"
	REDIS_PREFIX_AUTH     = "auth:"
	GIN_CASBIN            = "gin-shop"
	APP_REDIS_PREFIX_AUTH = "app_auth:"
	APP_AUTH_USER         = "app_auth_user:"
	CRON_KEY              = "cron:"
	DB_PERFIX             = "mt_gs_"
)
