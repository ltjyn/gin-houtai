package logging

import (
	"fmt"
	"gin/utils/global"
	"time"
)

// getLogFilePath get the log file save path
func getLogFilePath() string {
	return fmt.Sprintf("%s%s", global.GIN_CONFIG.App.RuntimeRootPath, global.GIN_CONFIG.App.LogSavePath)
}

// getLogFileName get the save name of the log file
func getLogFileName() string {
	return fmt.Sprintf("%s%s.%s",
		global.GIN_CONFIG.App.LogSaveName,
		time.Now().Format(global.GIN_CONFIG.App.TimeFormat),
		global.GIN_CONFIG.App.LogFileExt,
	)
}
