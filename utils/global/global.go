package global

import (
	"gin/conf"
	"github.com/spf13/viper"
	"go.uber.org/zap"
	"gorm.io/gorm"
)

var (
	GIN_DB     *gorm.DB
	GIN_VP     *viper.Viper
	GIN_LOG    *zap.SugaredLogger
	GIN_CONFIG conf.Config
)
